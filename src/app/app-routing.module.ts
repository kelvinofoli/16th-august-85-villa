import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AllPropertiesComponent} from './all-properties/all-properties.component'
import {PropertyComponent} from './property/property.component'
import {HomeComponent} from './home/home.component'
import {AboutComponent} from './about/about.component'

const routes: Routes = [

  {path: '', component: HomeComponent},
  {path: 'all', component: AllPropertiesComponent},
  {path: 'about', component: AboutComponent},
  {path: 'property/:id',component: PropertyComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
