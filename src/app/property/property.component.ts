import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router'
@Component({
  selector: 'app-property',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.css']
})
export class PropertyComponent implements OnInit {
  id: string
  images ;
  constructor(private route: ActivatedRoute) { }
  propertyData  = [
    {name: 'Property name',price:'Price',description:'description ',rooms: '4 BD', BAs:'not',images:1},
    {name: 'East Legon Hills',price:'$140,000',description:'Status: Newly Built ',rooms: '4 BD', BAs:'not',images:6},
    {name: 'Appiadu (landmark KNUST)',price:'GHc 700,000',
    description: `1. Bedrooms--4${'\n'}2. Washrooms--5${'\n'}3. Kitchen--YES${'\n'}4. Purpose--Residential${'\n'}5. Location--Appiadu (land mark KNUST)`,rooms: '4 BD', BAs:'5 BA',images:6},
    {name: 'Ahinsen (Landmark Kumasi High SHS/KNUST)',price:'GHc 900,000',description:`1. Bedrooms--4${'\n'}2. Washrooms--4.5${'\n'}3. Kitchen--YES${'\n'}4. Purpose--Residential${'\n'}5. Budget--900k GHc (Payment plan maybe accepted depending on the aggreement of payments)${'\n'}6. Ahinsen (Land mark Kumasi High SHS/KNUST)`,rooms: '4 BD', BAs:'4.5 BA',images:6},
    {name: 'Old Ashongman-Accra, 15 minutes drive from Legon Botanical Gardens.',price:'GHc 420,000',description:`3 Bedrooms Detached House For Sale,${'\n'}Location: Old Ashongman-Accra, 15 minutes drive from Legon Botanical Gardens.`,rooms: '3 BD', BAs:'not',images:6},
    {name: 'Oyarifa',price:'$250,000',description:`4 Bedrooms furnished house for sale at Oyarifa`,rooms: '4 BD', BAs:'not',images:6},
    {name: 'At East legon- School Junction (with 1 Boys quaters)',price:'$250,000',description:`Executive 5 Bedeoom${'\n'}House with  1 Boys quaters and a swimming pool for sale  At East legon- School Junction${'\n'}#Price $ 250,000${'\n'}Slightly Negotiable${'\n'}#Status  Newly Built`,rooms: '5 BD', BAs:'not',images:6},
    {name: '4 Bedroom House for sale at Roman Ridge ',price:'$450,000',description:`All bedrooms ensuite${'\n'}*Air-conditioned rooms${'\n'}*Wardrobes available${'\n'}*Polytank provided${'\n'}*Large and spacious compound${'\n'}*Beautiful Landscaping with Green grass and a Garden.${'\n'}Close to Roadside for easy access.In a very secure area.`,rooms: '4 BD', BAs:'not',images:6},
    {name: 'Luxurious 3 Bedroom House For Sale At East Legon Hills',price:'$110,000',description:`Luxurious 3 Bedroom House For Sale At East Legon Hills${'\n'}located In A Gated Community ${'\n'}just A Min Drive Away From The Main Road.${'\n'}- House Is Fully Walled And Gated With Electric Fencing ${'\n'}Paved Compound With Lawn Area.${'\n'}- Ample Parking Space ${'\n'},Air Conditioners In All Rooms${'\n'}Burglarproofed Sliding Windows${'\n'}Very Large Room Sizes${'\n'}Ultramodern Lighting Systems${'\n'}- Inbuilt Wall Wardrobes In All Rooms${'\n'}- All Rooms Well Tiled${'\n'}- Spacious Kitchens With Fitted Sinks, Cabinets And Storeroom `,rooms: '3 BD', BAs:'not',images:6},
    {name: '5 Bedroom House for sale at Roman Ridge ',price:'$110,000',description:`*All bedrooms ensuite${'\n'}*Air-conditioned rooms${'\n'}*Wardrobes available${'\n'}*Polytank provided${'\n'}*Large and spacious compound${'\n'}*Beautiful Landscaping with Green grass and a Garden.${'\n'}Close to Roadside for easy access.${'\n'}In a very secure area.`,rooms: '5 BD', BAs:'not',images:6},
    {name: '4 bedrooms town house letting in East Legon, Accra.',price:'Contact for Price...',description:`An exclusive community of 4 Bedroom Townhouses in East Legon’s most primed neighbourhood, just off Mensah Wood Street, behind Galaxy International School.${'\n'}The development offers 24 hour security, back-up utilities and swimming pool. Each house provides 4 double bedrooms and one single room, plus stylish open-plan living spaces and private terrace garden.`,rooms: '4 BD', BAs:'not',images:6},
    {name: '80% Completed 12 Apartments with 3 bedrooms each',price:'$750,000',description:`80% Completed 12 Apartments with 3 bedrooms each, with large compound For Sale at Kwadaso Hilltop Estate Kumasi Ghana.`,rooms: '4 BD x 12', BAs:'not',images:6},
  ]
  prop ;
  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get('id')
      // this.id = '0'
    })
    this.images = [1,2,3,4,5,6].map((n) => `../../assets/images/apartments/${this.id}/${n}.jpg`);
    this.prop = this.propertyData[this.id]
  }
  
}
